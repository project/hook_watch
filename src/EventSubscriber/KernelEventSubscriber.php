<?php

namespace Drupal\hook_watch\EventSubscriber;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\KernelEvents;
use Yosymfony\ResourceWatcher\Crc32ContentHash;
use Yosymfony\ResourceWatcher\ResourceCachePhpFile;
use Yosymfony\ResourceWatcher\ResourceWatcher;

/**
 * Main event subscriber.
 *
 * @package Drupal\hook_watch\EventSubscriber
 */
class KernelEventSubscriber implements EventSubscriberInterface {

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * Default cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $defaultCache;

  /**
   * Bootstrap cache.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  private $bootstrapCache;

  /**
   * Filesystem service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * Logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  private $loggerChannelFactory;

  /**
   * KernelEventSubscriber constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module handler service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $defaultCache
   *   Default cache.
   * @param \Drupal\Core\Cache\CacheBackendInterface $bootstrapCache
   *   Bootstrap cache.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   Filesystem service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   Logger channel factory.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler,
    CacheBackendInterface $defaultCache,
    CacheBackendInterface $bootstrapCache,
    FileSystemInterface $fileSystem,
    LoggerChannelFactoryInterface $loggerChannelFactory
  ) {
    $this->moduleHandler = $moduleHandler;
    $this->defaultCache = $defaultCache;
    $this->bootstrapCache = $bootstrapCache;
    $this->fileSystem = $fileSystem;
    $this->loggerChannelFactory = $loggerChannelFactory;
  }

  /**
   * Executed at the beginning of every request.
   *
   * @todo Only clear the specific cache items that we need.
   */
  public function onRequest() {
    $finder = new Finder();
    $finder->files()
      ->name('*.module')
      ->name('*.theme')
      ->name('*.profile')
      ->name('*.install')
      ->in(DRUPAL_ROOT . '/modules/')
      ->in(DRUPAL_ROOT . '/themes/')
      ->in(DRUPAL_ROOT . '/profiles/');

    $groups = [];
    foreach ($this->moduleHandler->getHookInfo() as $hook => $info) {
      $groups[$info['group']] = TRUE;
    }
    foreach ($groups as $group => $value) {
      $finder->name('*.' . $group . '.inc');
    }

    try {
      $hashContent = new Crc32ContentHash();
      $resourceCache = new ResourceCachePhpFile($this->fileSystem
        ->realpath('private://') . '/hook_watch_cache.php');
      $watcher = new ResourceWatcher($resourceCache, $finder, $hashContent);
      $watcher->initialize();

      $result = $watcher->findChanges();
      if ($result->hasChanges()) {
        $this->defaultCache->deleteAll();
        $this->bootstrapCache->deleteAll();
      }
    }
    catch (\InvalidArgumentException $exception) {
      // Cache file is probably corrupted.
      $this->loggerChannelFactory->get('hook_watch')
        ->warning($exception->getMessage());
    }
  }

  /**
   * Subscribe to events.
   *
   * @return array
   *   Subscribed events
   */
  public static function getSubscribedEvents(): array {
    return [
      // Set high priority value to start as early as possible.
      KernelEvents::REQUEST => ['onRequest', 1001],
    ];
  }

}
